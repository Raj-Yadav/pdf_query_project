import os
from llama_index import VectorStoreIndex, SimpleDirectoryReader, ServiceContext
from llama_index.embeddings import HuggingFaceEmbedding
from llama_index.llms import HuggingFaceLLM, HuggingFaceInferenceAPI

from config import API_TOKEN

embed_model_name = "BAAI/bge-small-en-v1.5"
llm_model_name = "HuggingFaceH4/zephyr-7b-alpha"

def get_embed_model(embed_model_name):
    embed_model = HuggingFaceEmbedding(model_name=embed_model_name)
    return embed_model


def get_llm_model(llm_model_name):
    llm_model = HuggingFaceInferenceAPI(model_name=llm_model_name, 
                                    token=API_TOKEN)
    return llm_model


def read_data(dir_path):
    if os.path.isdir(dir_path):
        return SimpleDirectoryReader(dir_path).load_data()
    else:
        dir_path = os.path.dirname(os.path.realpath(dir_path))
        return SimpleDirectoryReader(dir_path).load_data()
    


def create_query_engine(dir_path):
    document = read_data(dir_path)
    embed_model = get_embed_model(embed_model_name)
    llm_model = get_llm_model(llm_model_name)

    service_context = ServiceContext.from_defaults(
        embed_model=embed_model,llm=llm_model
    )
    index = VectorStoreIndex.from_documents(
    documents=document, service_context=service_context
    )
    query_engine = index.as_query_engine()

    return query_engine


def query_question(question, query_engine):
    response = query_engine.query(question)
    return response


if __name__ == "__main__":
    files_path = os.getcwd() + "\data"
    document = read_data(files_path)
    query_engine = create_query_engine(document)
    response = query_question("when was google company established?", query_engine)
    # response = query_engine.query("when was google company established?")
    print(response)
