# Application Documentation

## Introduction
This document provides an overview and usage guide for the "TALK WITH YOUR DATA" application. The application allows users to interact with their own data by asking questions and receiving relevant answers based on the provided data.

## Functionality
The application consists of two main components:
1. **llamaIndex Code**: This component is responsible for processing and indexing the provided data to enable efficient querying.
2. **Gradio Code**: This component creates a user interface where users can upload their data files, ask questions, and receive answers.

## Usage
### Step 1: Setup
1. Clone the repository containing the application code.
2. Install the necessary dependencies using `pip install -r requirements.txt`.
3. Ensure that you have the required data files in the specified format.

### Step 2: Running the Application
#### Running the llamaIndex Code
- Ensure that the `llama_index` library is installed. If not, install it using `pip install llama_index`.
- Set up the necessary configurations in the `config.py` file, including the `API_TOKEN`.
- Run the `llamaIndex.py` script to index the provided data.

#### Running the Gradio Code
- Ensure that the `gradio` library is installed. If not, install it using `pip install gradio`.
- Run the `gradio_interface.py` script to start the Gradio interface.

### Step 3: Interacting with the Application
1. **Upload Data**: Use the "Your Files" button to upload your data files.
2. **Create Query Engine**: Click the "Create Query Engine" button to initialize the query engine with the uploaded data.
3. **Ask Question**: Enter your question in the "Question" textbox and click "Submit".
4. **View Answer**: The answer to your question will be displayed in the "Your Answer" textarea.

## Example
Suppose you have a dataset containing information about companies. You can upload this dataset, create a query engine, and then ask questions about the companies in the dataset, such as their establishment dates or locations.

## Note
- Ensure that your data files are in the appropriate format and contain the necessary information for accurate querying.
- The accuracy of the answers depends on the quality of the data and the effectiveness of the indexing and querying algorithms.
