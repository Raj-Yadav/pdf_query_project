import gradio as gr
from ml import create_query_engine, read_data, query_question

title = "TALK WITH YOUR DATA"

desc = "This project help you to talk with your own data."

with gr.Blocks(title=title, head=desc) as demo:
    
    with gr.Row():
        with gr.Column(scale=2):
            input_file = gr.File(label="Your Files")
            query_engine_btn = gr.Button("Create Query Engine")
            output_function = gr.State([])
                   
        with gr.Column(scale=2):
            question_text = gr.Textbox(label="Question", placeholder="Ask your Questions . . .", lines=3)
            submit_btn = gr.Button("Submit")
            

    with gr.Column():
        answer_text = gr.TextArea(label="Your Answer")
            
    query_engine_btn.click(create_query_engine, inputs=[input_file], outputs=[output_function])
    submit_btn.click(query_question, inputs=[question_text, output_function], outputs=[answer_text])
if __name__ == "__main__":  
    
    demo.launch() 